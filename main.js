const rods = document.querySelectorAll(".rods")
const winningRod = document.getElementById("winning-rod")

let clicked = false
let tempArray = []
rods.forEach((rod) => {
    rod.addEventListener('click', clickRod)
})

function clickRod(event) {
    if (clicked === false) {
        let ringElement = event.currentTarget.lastElementChild
        tempArray.push(ringElement)
        clicked = true;
    }
    else {

        clicked = false

        if (event.currentTarget.lastElementChild) {
            let lastElementChildwidth = event.currentTarget.lastElementChild.offsetWidth
            let elementRingWidth = tempArray[0].offsetWidth
            if (lastElementChildwidth > elementRingWidth) {
                event.currentTarget.appendChild(tempArray[0])
                tempArray = []
            }
        }
        else {
            event.currentTarget.appendChild(tempArray[0])
            tempArray = []
        }
        tempArray = []
    }

        let rodCountOnWinningRod = winningRod.childElementCount;
        if (rodCountOnWinningRod == 4){
            window.alert("YOU WIN!!!!!!!!!!!!!!")
        }
    }